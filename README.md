Library Profile
================

This installation profile delivers a packaged set of modules, features, and a
custom zen sub-theme for use with Western Washington University Library site.
