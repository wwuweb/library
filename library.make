; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; -----------

projects[drupal][type] = core

; Collegesites profile
; --------------------

projects[collegesites][download][type] = "git"
projects[collegesites][download][tag] = "7.x-12.0.0"
projects[collegesites][download][url] = "https://bitbucket.org/wwuweb/collegesites.git"
projects[collegesites][type] = "module"

; Base Modules
; ------------

projects[context][version] = 3.10
projects[context][type] = "module"

projects[date][version] = 2.9
projects[date][type] = "module"

projects[elysia_cron][version] = 2.7
projects[elysia_cron][type] = "module"

projects[h5p][version] = 1.39
projects[h5p][type] = "module"

projects[hide_submit][version] = 2.4
projects[hide_submit][type] = "module"

projects[inline_entity_form][version] = 1.8
projects[inline_entity_form][type] = "module"

projects[panelizer][version] = 3.4
projects[panelizer][type] = "module"

projects[panels_extra_layouts][version] = 2.0
projects[panels_extra_layouts][type] = "module"

; Ancilliary Modules
; ------------------

projects[bundle_copy][version] = 1.1
projects[bundle_copy][type] = "module"

projects[cors][version] = 1.3
projects[cors][type] = "module"

projects[fontawesome][version] = 2.9
projects[fontawesome][type] = "module"

projects[markup][version] = 1.2
projects[markup][type] = "module"

projects[menu_block][version] = 2.7
projects[menu_block][type] = "module"

projects[nodeaccess][version] = 1.7
projects[nodeaccess][type] = "module"

projects[node_export][version] = 3.1
projects[node_export][type] = "module"

projects[noreqnewpass][version] = 1.2
projects[noreqnewpass][type] = "module"

projects[override_node_options][version] = 1.14
projects[override_node_options][type] = "module"

projects[permissions_per_webform][version] = 1.0-alpha1
projects[permissions_per_webform][type] = "module"

projects[services][version] = 3.25
projects[services][type] = "module"

projects[simplenews][version] = 1.1
projects[simplenews][type] = "module"

projects[simplenews_status_reset][version] = 1.0
projects[simplenews_status_reset][type] = "module"

projects[special_menu_items][version] = 2.1
projects[special_menu_items][type] = "module"

projects[tb_megamenu][version] = 1.0-rc2
projects[tb_megamenu][type] = "module"
projects[tb_megamenu][patch][] = "https://bitbucket.org/wwuweb/library-megamenu-patch/raw/e4b05029f457fdfd89a64e7e2b52d0a58cf57d89/tb_megamenu-library_custom_styles-7.x.patch"

projects[uuid][version] = 1.3
projects[uuid][type] = "module"

; Required to override patches from collegesites.
projects[webform_rules][version] = 1.6
projects[webform_rules][type] = "module"

projects[webform_service][version] = 4.0-beta2
projects[webform_service][type] = "module"

projects[webform_submission_uuid][version] = 1.0
projects[webform_submission_uuid][type] = "module"

projects[webform_uuid][version] = 1.3
projects[webform_uuid][type] = "module"

; Libraries
; ---------

libraries[fontawesome][download][type] = "file"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/v4.6.3.zip"
